#!/usr/bin/env bash

shopt -s expand_aliases
# shellcheck disable=SC1090
[[ -f ~/.bashrc ]] && . ~/.bashrc

# edit this variable to run it as another user (for example when using it locally)
_DEPLOY_USER=${DEPLOY_USER:-deploy}
_SECURITY_SCAN_IMAGE=${SECURITY_SCAN_IMAGE:-registry.gitlab.com/clarin-eric/docker-snyk-cli}
_SECURITY_SCAN_IMAGE_VERSION=${SECURITY_SCAN_IMAGE_VERSION:-0.0.9}
_SHELL_LINT_IMAGE=${SHELL_LINT_IMAGE:-koalaman/shellcheck-alpine}
_SHELL_LINT_IMAGE_VERSION=${SHELL_LINT_IMAGE_VERSION:-v0.9.0}
_YQ_IMAGE=${YQ_IMAGE:-mikefarah/yq}
_YQ_IMAGE_VERSION=${YQ_IMAGE_VERSION:-4.35.2}

main() {
    _check_prerequisites

    EXEC_NAME=""
    EXEC_CMD=""
    SINGLE=0 #Are we running in single mode, default is 0/false. This will enforce running the script as the deploy user
             #SINGLE=1/true will not use NAME as the project root. Instead it will use the directory from where this script was called (must be an ancestor directory of COMPOSE_CLARIN_DIR)

    PROJECT_NAME="."

    HELP=0
    VERBOSE=0
    ARG_REMAINDER_ARRAY=()

    #
    # Process script arguments and parse all optional flags
    #
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        -v|--verbose)
            VERBOSE=1
            VERBOSE_ARG="-v"
            ;;
        -h|--help)
            HELP=1
            ;;
        -s|--single)
            SINGLE=1
            ;;
        *)
            ARG_REMAINDER_ARRAY+=("$key")
            ;;
    esac
    shift # past argument or value
    done

    if [ "${SINGLE}" -eq 0 ]; then
        if [ "$(whoami)" != "${_DEPLOY_USER}" ]; then
            fatal "control.sh MUST be run as the \"${_DEPLOY_USER}\" user in multi project mode. Alternatively use \"${0#./} -s <opts>\" from the project directory to run in single project mode."
        fi
        if [ -n "${ARG_REMAINDER_ARRAY[0]}" ]; then
            PROJECT_NAME=${ARG_REMAINDER_ARRAY[0]}
        fi
    else
        info "Running in SINGLE project mode"
        if [ "$(whoami)" == "${_DEPLOY_USER}" ]; then
            warn "Script is being run by the ${_DEPLOY_USER} user in single project mode."
            ask_confirm "Are you sure you are in single project mode?"
        fi
    fi

    if [ "${HELP}" -eq 1 ]; then
        echo ""
        echo "Usage: ${0#./} [-h|-v] [<name>|-s] <command>"
        echo ""
        echo "  Required arguments"
        echo "    <name>                        Name of the project. Must exist as a direct subdirectory in the location where this command is run."
        echo "    <command>                     One of the available commands listed in the \"Commands\" section below."
        echo ""
        echo "  Optional flags:"
        echo "    -v, --verbose                 Run in verbose mode"
        echo "    -h, --help                    Show help"
        echo "    -s, --single                  Run in single project mode."
        echo ""
        echo "  Commands:"
        echo "    init                          Initialize the project, required before you can use the other commands."
        echo "    start                         Start (docker-compose up -d)"
        echo "    stop [-V]                     Stop (docker-compose down). -V to remove project volumes"
        echo "    restart                       Restart, runs stop and start"
        echo "    status                        Check status of all services"
        echo "    logs                          Tail logs of all services"
        echo "    pull                          Pull service images"
        echo "    exec <name> <command>         Exec the specified <command> in the service with <name>"
        echo ""
        echo "    backup                        Create a backup"
        echo "    restore                       Restore a backup"
        echo "    restore ls                    List available backups for restore"
        echo ""
        echo "    lint                          Lint project's shell scripts. Includes all *.sh and *.bash files"
        echo "    scan                          Scan project images for known security vulnerabilities. Includes images declared in .overlays"
        echo "    test                          Run project's test configuration"
        echo ""

        CUSTOM_SCRIPT=$(find "${PROJECT_NAME}" -maxdepth 2 -name 'custom.sh' -type f)
        CUSTOM_SCRIPT_SU=$(find "${PROJECT_NAME}" -maxdepth 2 -name 'custom-su.sh' -type f)
        if [ "${CUSTOM_SCRIPT}" != "" ]; then
            echo "  Custom commands:"
            ${CUSTOM_SCRIPT} --help
        fi
        if [ "${CUSTOM_SCRIPT_SU}" != "" ]; then
            echo "  Custom superuser commands:"
            CUSTOM_SCRIPT_SU=$(realpath "$CUSTOM_SCRIPT_SU")
            bash "${CUSTOM_SCRIPT_SU}" --help
        fi

        exit 0
    fi

    # Assuming a structure as follows:
    #   ...<a dir>/<project_dir>/<compose_dir>/clarin/docker-compose.yml
    #
    # In normal mode the control script can be run from:
    #
    #   <a dir>
    #
    # with a name supplied as the first positional argument, where name has to be a valid <project dir>
    #
    # In single mode (-s), the control script can also be run from:
    #
    #   <project_dir> or <compose_dir>
    #
    # for both cases the 1 or 2 level structure to docker-compose.yml is expected. An error is thrown if this structure
    # is not found.
    #
    # For all cases the do_work function is called in a subshell with the working directory set to <project_dir>
    #
    if [ "${SINGLE}" -eq 1 ] ; then

        if [ -f "${PROJECT_NAME}/clarin/docker-compose.yml" ]; then
            #Single project mode, check if we are in the root of a project directory
            #Set the project name to the current directory name and call the do_work function
            #in a subshell (to not change the working dir when the script is finished) from
            #the parent directory
            PROJECT_NAME=$(basename "$(pwd)")

            COMMAND=${ARG_REMAINDER_ARRAY[0]}
            COMMAND_ARGS=()
            for arg in "${ARG_REMAINDER_ARRAY[@]:1}"
            do
                COMMAND_ARGS+=("$arg")
            done

            ( cd .. && do_work )
        elif [ "$(find . -maxdepth 3 -name docker-compose.yml | awk -F/ '{print $4}')" == "docker-compose.yml" ]; then
            #Todo: matching on slash could be dangerous with escaped characters in paths?
            info "Special case"
            PROJECT_NAME="."

            COMMAND=${ARG_REMAINDER_ARRAY[0]}
            COMMAND_ARGS=()
            for arg in "${ARG_REMAINDER_ARRAY[@]:1}"
            do
                COMMAND_ARGS+=("$arg")
            done

            ( do_work )
        else
            #
            #TODO: improve this message
            #
            error "The ${PROJECT_NAME} directory does not appear to be a valid project directory."
            error "File [${PROJECT_NAME}/clarin/docker-compose.yml] does not exits."
            error ""
            error "A common solution to fix this issue is to make sure the [${PROJECT_NAME}] directory"
            error "has a parent directory where it can store static, non versioned, files"
            error ""
            exit 1
        fi
    else
        #Normal mode, require a name parameter
        COMMAND=${ARG_REMAINDER_ARRAY[1]}
        COMMAND_ARGS=()
        for arg in "${ARG_REMAINDER_ARRAY[@]:2}"
        do
            COMMAND_ARGS+=("$arg")
        done

        #PROJECT_NAME parameter is required and must be a subdirectory in multi project mode (deploy scenario)
        if [ "${PROJECT_NAME}" == "." ]; then
            fatal "PROJECT_NAME is required"
        fi
        if [ ! -d "${PROJECT_NAME}" ]; then
            fatal "${PROJECT_NAME} not found"
        fi

        #Do the actual work
        ( cd "${PROJECT_NAME}" && do_work )
    fi
}

_check_prerequisites() {
    if [ -f /etc/docker/daemon.json ] && [ "$(command -v jq)" == "" ]; then
        error "Required command jq not installed"
        exit 1
    fi

    if [ "$(command -v stdbuf)" == "" ]; then
        error "Required command stdbuf not installed"
        error "On OSX run:"
        error "1. brew install coreutils"
        error "2. (cd /usr/local/bin && sudo ln -s ../opt/coreutils/libexec/gnubin/stdbuf stdbuf)"
        exit 1
    fi
}

setup_overlays() {
    info "Overlays check working directory: $(pwd)"
    if [ -f "${1}" ]; then
        info "Using overlays"
        while read -r line
        do
            overlay=$(echo "${line}" | grep -v -e "^#")
            if [ "${overlay}" != "" ]; then
                if [ -f "${COMPOSE_CLARIN_DIR}/${overlay}.yml" ]; then
                    info "  Enabling: ${overlay}"
                    COMPOSE_OPTS+=" -f ${overlay}.yml"
                else
                    warn "Compose overlay file \"${overlay}\" not found. Declared in: ${1}"
                fi
            fi
        done < "${1}"
    else
        warn "Overlays file not found"
    fi
}

do_work() {
    info "PROJECT_NAME 2=${PROJECT_NAME}, Working dir: $(pwd)"

    #Search for list of matching dirs and process list of matched dirs to check if there is one
    # with a compose file
    COMPOSE_DIR=$(find . -maxdepth 2 -name 'clarin' -type d -exec bash -c 'if [ -f $1/docker-compose.yml ]; then realpath $(dirname $1); fi' shell {} \;)

    if [ "${COMPOSE_DIR}" == "" ]; then

        if [ "${SINGLE}" == "1" ]; then
            error "No valid compose directory found"
            error "Deep directory structure required"
        else
            fatal "No valid compose directory found"
        fi

    else
        info "COMPOSE_DIR=${COMPOSE_DIR}"
    fi

    COMPOSE_CLARIN_DIR="${COMPOSE_DIR}/clarin"
    COMPOSE_YAML="${COMPOSE_CLARIN_DIR}/docker-compose.yml"
    BACKUPS_DIR="${COMPOSE_DIR}/../backups"
    TEST_DIR="${COMPOSE_DIR}/test"
    COMPOSE_OVERRIDE_TEST_FILE="${TEST_DIR}/compose-test-override.yml"
    CUSTOM_START_SCRIPT="${COMPOSE_DIR}/start.sh"
    CUSTOM_STOP_SCRIPT="${COMPOSE_DIR}/stop.sh"
    BACKUP_SCRIPT="${COMPOSE_DIR}/backup.sh"
    RESTORE_SCRIPT="${COMPOSE_DIR}/restore.sh"
    LIST_BACKUPS_SCRIPT="${COMPOSE_DIR}/list_backups.sh"
    LINT_SCRIPT="${COMPOSE_DIR}/lint.sh"
    SCAN_SCRIPT="${COMPOSE_DIR}/scan.sh"
    TEST_SCRIPT="${COMPOSE_DIR}/test.sh"
    ENV_TEMPLATE_FILE="${COMPOSE_CLARIN_DIR}/.env-template"
    OVERLAYS_TEMPLATE_FILE="${COMPOSE_CLARIN_DIR}/.overlays-template"
    OVERLAYS_FILE="${COMPOSE_DIR}/../.overlays"
    OVERLAYS_TEST_FILE="${COMPOSE_DIR}/test/.overlays"
    BACKUPS_TIMESTAMP=$(date +%Y%m%dT%H%M%S)

    COMPOSE_OPTS="-f docker-compose.yml"
    COMPOSE_CMD_ARGS=""

    #Check that there is a docker-compose.yml
    if [ ! -f "${COMPOSE_YAML}" ]; then
        fatal "${COMPOSE_YAML} does not exist!"
    fi

    info "PROJECT_NAME=${PROJECT_NAME}, COMMAND=${COMMAND}"
    CUSTOM_COMMAND=""
    case $COMMAND in
        init)
            setup_overlays "${OVERLAYS_FILE}"
            init
            exit 0
            ;;
        start)
            setup_overlays "${OVERLAYS_FILE}"
            debug "Start. COMPOSE_OPTS: <${COMPOSE_OPTS}> COMPOSE_CMD_ARGS: <${COMPOSE_CMD_ARGS}>"
            start
            exit 0
            ;;
        stop)
            setup_overlays "${OVERLAYS_FILE}"
            debug "Stop. COMPOSE_OPTS: <${COMPOSE_OPTS}> COMPOSE_CMD_ARGS: <${COMPOSE_CMD_ARGS}>"
            stop
            exit 0
            ;;
        restart)
            setup_overlays "${OVERLAYS_FILE}"
            debug "Restart: stop [COMPOSE_OPTS: <${COMPOSE_OPTS}> COMPOSE_CMD_ARGS: <${COMPOSE_CMD_ARGS}>]"
            stop
            debug "Restart: start [COMPOSE_OPTS: <${COMPOSE_OPTS}> COMPOSE_CMD_ARGS: <${COMPOSE_CMD_ARGS}>]"
            start
            exit 0
            ;;
        backup)
            setup_overlays "${OVERLAYS_FILE}"
            backup
            exit 0
            ;;
        restore)
            setup_overlays "${OVERLAYS_FILE}"
            restore
            exit 0
            ;;
        status)
            setup_overlays "${OVERLAYS_FILE}"
            _run_compose_command "ps"
            exit 0
            ;;
        logs)
            setup_overlays "${OVERLAYS_FILE}"
            COMPOSE_CMD_ARGS="-f"
            _run_compose_command "logs"
            exit 0
            ;;
        exec)
            setup_overlays "${OVERLAYS_FILE}"
            do_exec
            exit 0
            ;;
        pull)
            setup_overlays "${OVERLAYS_FILE}"
            _run_compose_command "pull"
            exit 0
            ;;
        lint)
            setup_overlays "${OVERLAYS_FILE}"
            lint
            exit 0
            ;;
        scan)
            setup_overlays "${OVERLAYS_FILE}"
            scan
            ;;
        test)
            test
            exit 0
            ;;
        *)
            setup_overlays "${OVERLAYS_FILE}"
            CUSTOM_COMMAND="$COMMAND"
            ;;
     esac

    #We have not exited, check if we can run as a custom subcommand
    export COMPOSE_OPTS
    CUSTOM_SCRIPT=$(find . -maxdepth 2 -name 'custom.sh' -type f)
    if [ "${CUSTOM_SCRIPT}" != "" ]; then
        info "Delegating to: ${PROJECT_NAME}/${CUSTOM_SCRIPT#./} ${CUSTOM_COMMAND} ${COMMAND_ARGS[*]}"
        _run_command "custom.sh" "${CUSTOM_SCRIPT} ${CUSTOM_COMMAND} ${COMMAND_ARGS[*]} ${VERBOSE_ARG}"
        RETURN=$?
    else
        RETURN=1
        info "custom.sh not defined"
    fi

    if [ ! "${RETURN}" -eq 0 ]; then
        CUSTOM_SCRIPT_SU=$(find . -maxdepth 2 -name 'custom-su.sh' -type f)
        if [ "${CUSTOM_SCRIPT_SU}" != "" ]; then
            info "Delegating to: ${PROJECT_NAME}/${CUSTOM_SCRIPT_SU#./} ${CUSTOM_COMMAND} ${COMMAND_ARGS[*]}"
            CUSTOM_SCRIPT_SU=$(realpath "$CUSTOM_SCRIPT_SU")              # This is to allow more fine grained (path restricted) sudo rules
            _run_command "custom.sh superuser" "bash ${CUSTOM_SCRIPT_SU} ${CUSTOM_COMMAND} ${COMMAND_ARGS[*]} ${VERBOSE_ARG}"
        else
            info "custom-su.sh not defined"
        fi
    fi

}

create_env_link_to_current() {    
    # Create the .env symlink relative to the current directory (SINGLE=0 => .=NAME | SINGLE=1 => .= <path from where this script was called>)
    REL_PATH_TO_CURRENT=${COMPOSE_CLARIN_DIR#"$(realpath .)"} # Get the relative path of COMPOSE_CLARIN_DIR to the current directory
    REL_PATH_TO_CURRENT=${REL_PATH_TO_CURRENT#"."} # Remove potential leading "."
    IFS=" " read -r -a REL_PATH_TO_CURRENT_ARR <<< "${REL_PATH_TO_CURRENT//\// }" # Create an array spliting the path on "/"
    for dir in "${REL_PATH_TO_CURRENT_ARR[@]}" # Iterate the array and build the reverse relative path
    do
        if [ "$dir" !=  "${REL_PATH_TO_CURRENT_ARR[0]}" ]; then
            LINK_REL_PATH+=/
        fi
        LINK_REL_PATH+=..
    done
    debug "Relative path for .env symlink: LINK_REL_PATH=${LINK_REL_PATH}"
    (cd "${COMPOSE_CLARIN_DIR}" && ln -s "${LINK_REL_PATH}/.env" ".env")
}

template_variable() {
    PATTERN=$1
    VALUE=$2
    FILE=$3

    #Templating
    unamestr=$(uname)
    if [[ "$unamestr" == 'Linux' ]] || sed --version 2>/dev/null |grep -q "GNU sed"; then # Account for other platforms with GNU sed installed 
        info "Templating ${PATTERN} in ${FILE} file (platform=linux)"
        sed -i "s/${PATTERN}/${VALUE}/g" "${FILE}"
    elif [[ "$unamestr" == 'Darwin' ]]; then
        info "Templating ${PATTERN} in ${FILE} file (platform=darwin)"
        sed -i "" -e "s/${PATTERN}/${VALUE}/g" "${FILE}"
    else
        fatal "Unsupported platform: ${unamestr}"
    fi
}

init() {
    info "init, working dir=$(pwd), compose_clarin_dir=${COMPOSE_CLARIN_DIR}"
    if [ ! -e "${COMPOSE_CLARIN_DIR}/.env" ]; then
        #Validate backups directory
        if [ ! -d "${BACKUPS_DIR}" ]; then
            info "Creating ${BACKUPS_DIR} directory"
            mkdir -p "${BACKUPS_DIR}"
        else
            info "${BACKUPS_DIR} directory exists"
        fi

        #Validate .env file and create + template if needed
        if [ ! -f "${ENV_TEMPLATE_FILE}" ]; then
            fatal "Missing .env template file (${ENV_TEMPLATE_FILE})"
        fi

        info "Copying ${ENV_TEMPLATE_FILE} to .env"
        cp "${ENV_TEMPLATE_FILE}" ".env"
        chmod 600 ".env"

        set_service_level

        #Symlink .env file if needed (can be caused by redeploys)
        if [ ! -L "${COMPOSE_CLARIN_DIR}/.env" ]; then
            info "Missing .env symlink. Creating new ${COMPOSE_CLARIN_DIR}/.env --> ${PWD}/.env symlink"
            create_env_link_to_current
        fi

        #Generate random passwords for up to 10 password variables
        COUNTER=0
        while [  $COUNTER -lt 10 ]; do
            if [ "$(grep -c "{{GENERATE_PASSWORD_${COUNTER}}}" .env)" -eq 1 ]; then
                RANDOM_VALUE=$(head /dev/urandom | LC_ALL=C tr -dc A-Za-z0-9 | head -c 13 ; echo '')
                template_variable "{{GENERATE_PASSWORD_${COUNTER}}}" "${RANDOM_VALUE}" ".env"
            fi
            COUNTER=$((COUNTER+1))
        done

        #Create .overlays file if needed
        if [ ! -f ".overlays" ]; then
            if [ -f "${OVERLAYS_TEMPLATE_FILE}" ]; then
                info "Copying ${PROJECT_NAME}/${OVERLAYS_TEMPLATE_FILE} to ${PROJECT_NAME}/${OVERLAYS_FILE}"
                cp "${OVERLAYS_TEMPLATE_FILE}" "${OVERLAYS_FILE}"
            fi
        fi
    else
    #TODO: if we automate updating this we should also handle restarting the project and take into account changed volume
    #names
#        project_name_prefix=$(grep "COMPOSE_PROJECT_NAME" "${COMPOSE_CLARIN_DIR}/.env" | awk -F "=" '{print $2}' | awk -F "_" '{print $1}')
#        if [ "${project_name_prefix}" != "dev" ] && [ "${project_name_prefix}" != "staging" ] && [ "${project_name_prefix}" != "prod" ]; then
#            set_service_level
#      else
            info ".env file exists, skipping initialization"
#        fi
    fi
}

set_service_level() {
    SERVICE_LEVEL="dev"
    read -r -e -p $'Please specify the desired level for this service.\n [1] - development\n [2] - staging\n [3] - production\n [1|2|3]> ' response
    case "$response" in
        2|staging)
            SERVICE_LEVEL="staging"
            ;;
        3|production)
            SERVICE_LEVEL="prod"
            ;;
        1|development)
            ;;

        *)
            warn "Invalid service level. Using default \"[1] - Development\""
            ;;
    esac

    sed -i "s/^COMPOSE_PROJECT_NAME=/COMPOSE_PROJECT_NAME=${SERVICE_LEVEL}_/g" ".env"
}

start() {
    if [ "$1" != "test" ]; then
        if [ ! -e "${COMPOSE_CLARIN_DIR}/.env" ] && [ ! -e "${COMPOSE_CLARIN_DIR}/.env" ]; then
            if [ ! -f .env ]; then
                fatal "Missing .env file, initialize first"
            fi
            #Symlink .env file if needed (can be caused by redeploys)
            if [ ! -L "${COMPOSE_CLARIN_DIR}/.env" ]; then
                info "Missing .env symlink. (Re)Creating new ${COMPOSE_CLARIN_DIR}/.env --> ${PWD}/.env symlink"
                create_env_link_to_current
            fi
        fi
        COMPOSE_CMD_ARGS="-d"
    elif [ -f "${CUSTOM_START_SCRIPT}" ]; then
        EXTRA_ARGS="--foreground"
    fi

    # Check mtu settings
    #
    # Docker c ompose does not properly follow the daemon mtu setting. As a workaround we required the existince of this network-override.yml to 
    # specifically apply the custom mtu setting on a per project basis.
    # References:
    # - https://github.com/docker/compose/issues/3438
    # - https://mlohr.com/docker-mtu/
    #
    if [ -f /etc/docker/daemon.json ]; then
        mtu=$(jq '.mtu' /etc/docker/daemon.json)
        if [ "${mtu}" != "null" ]; then
            info "Detected non standard MTU configuration"
            if [ -f "${COMPOSE_CLARIN_DIR}/network-override.yml" ]; then
                if [ ! -f .overlays ]; then
                    #create
                    info "Creating network override overlay for non standard MTU configuration"
                    echo "network-override" >> .overlays
                else
                    #update
                    if [ "$(grep -c "network-override" .overlays)" == "0" ]; then
                        info "Enabling network override overlay for non standard MTU configuration"
                        echo "network-override" >> .overlays
                    else
                        info "Network override overlay for non standard MTU configuration already enabled"
                    fi
                fi
            else
                error "Non standard MTU configuration (${mtu}) detected but no overlays file detected."
                error "Please provide a network overlay file: ${COMPOSE_CLARIN_DIR}/network-override.yml"
                #TODO: continue or exit here?
            fi
        else
            info "Default mtu value detected"
        fi
    fi
    if [ -f "${CUSTOM_START_SCRIPT}" ]; then
        info "Running custom start script: ${CUSTOM_START_SCRIPT}"
        debug "\"${CUSTOM_START_SCRIPT}\" \"${COMPOSE_OPTS}\" \"${COMPOSE_CMD_ARGS}\" \"${EXTRA_ARGS}\" \"${VERBOSE_ARG}\""
        shell_args="COMPOSE_DIR=${COMPOSE_DIR} COMPOSE_OPTS=\"${COMPOSE_OPTS}\" COMPOSE_CMD_ARGS=\"${COMPOSE_CMD_ARGS}\""
        _run_command "custom start" "${shell_args} ${CUSTOM_START_SCRIPT} \"${COMPOSE_OPTS}\" \"${COMPOSE_CMD_ARGS}\" ${EXTRA_ARGS} ${VERBOSE_ARG}" 
    else
        _run_compose_command "up"
    fi
}

stop() {
    if [ -n "${COMMAND_ARGS[0]}" ]; then
        if [ "${COMMAND_ARGS[0]}" == "-V" ]; then
            COMPOSE_CMD_ARGS="-v"
        else
            fatal "parameter \"${COMMAND_ARGS[0]}\" not valid for \"stop\" command"
        fi
    fi
    if [ -f "${CUSTOM_STOP_SCRIPT}" ]; then
        info "Running custom stop script: ${CUSTOM_STOP_SCRIPT}"
        debug "\"${CUSTOM_STOP_SCRIPT}\" \"${COMPOSE_OPTS}\" \"${COMPOSE_CMD_ARGS}\" \"${VERBOSE_ARG}\""
        shell_args="COMPOSE_DIR=${COMPOSE_DIR} COMPOSE_OPTS=\"${COMPOSE_OPTS}\" COMPOSE_CMD_ARGS=\"${COMPOSE_CMD_ARGS}\""
        _run_command "custom stop" "${shell_args} ${CUSTOM_STOP_SCRIPT} \"${COMPOSE_OPTS}\" \"${COMPOSE_CMD_ARGS}\" ${EXTRA_ARGS} ${VERBOSE_ARG}" 
    else
        info "Running stop command, cwd=$(pwd)"
        _run_compose_command "down"
    fi
}

backup() {
    info "Running backup command, cwd=$(pwd), backup_script=${BACKUP_SCRIPT}"
    if [ ! -f "${BACKUP_SCRIPT}" ]; then
        fatal "Backup script (${PROJECT_NAME}/${BACKUP_SCRIPT}) does not exist"
    fi

    BACKUP_ARGS="${COMMAND_ARGS[*]}"

    info "Creating backup: pwd=$(pwd), name=${PROJECT_NAME}"
    shell_args="COMPOSE_DIR=${COMPOSE_DIR} BACKUPS_DIR=${COMPOSE_DIR}/../backups BACKUPS_TIMESTAMP=${BACKUPS_TIMESTAMP}"
    if ! _run_command "backup" "${shell_args} ${BACKUP_SCRIPT} ${BACKUP_ARGS} ${VERBOSE_ARG}"; then
    	error "Backup failed"
    	exit 1
    fi
}

restore() {
    info "Running restore command, cwd=$(pwd), restore_script=${RESTORE_SCRIPT}"
    if [ ! -f "${RESTORE_SCRIPT}" ]; then
        fatal "Restore script (${PROJECT_NAME}/${RESTORE_SCRIPT}) does not exist"
    fi

    RESTORE_ARGS=${COMMAND_ARGS[*]}
    #Get array last index, ${COMMAND_ARGS[-1] gave a "COMMAND_ARGS: bad array subscript" error on my macbook
    #Broken on OSX (bash --version):
    #  GNU bash, version 3.2.57(1)-release (arm64-apple-darwin21)
    #  Copyright (C) 2007 Free Software Foundation, Inc.
    #
    #Working on OSX (bash --version):
    #  GNU bash, version 5.2.26(1)-release (aarch64-apple-darwin23.2.0)
    #  Copyright (C) 2022 Free Software Foundation, Inc.
    #  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
    #
    #  This is free software; you are free to change and redistribute it.
    #  There is NO WARRANTY, to the extent permitted by law.
    RESTORE_ARGS_SIZE=${#RESTORE_ARGS[@]}
    RESTORE_ARGS_LAST_IDX=$((RESTORE_ARGS_SIZE-1))

    if [ -z "${RESTORE_ARGS[0]}" ]; then
        error "Backup identifier not supplied"
        lsbackups
        fatal "Aborting now"
    fi
	
    # Check for supported arguments:
    # by default assume a backup filename is supplied
    # if the first argument is one of:
    # ls		list the available backups
    # latest		try to set the backup filename by using the latest available file
    # <timestamp> 	if the argument matches the timestamp regex, set the backup filename based on the supplied timestamp
    RESTORE_FILE="${RESTORE_ARGS[0]}"
    if [ "${RESTORE_ARGS[0]}" == "ls" ]; then
        lsbackups
        exit 0
    elif [ "${RESTORE_ARGS[0]}" == "latest" ]; then
        info "Restore latest"
        LATEST_FILE=$(unset -v latest; for file in "${COMPOSE_DIR}"/../backups/*; do [ ! -L "$file" ] && [[ $file -nt $latest ]] &&  latest=$file; done; echo "$latest")
        RESTORE_FILE="${LATEST_FILE}"
    elif is_timestamp_valid "${COMMAND_ARGS[${RESTORE_ARGS_LAST_IDX}]}"; then # we assume the timestamp to always be the last argument
        info "Restore by timestamp"
        RESTORE_FILE=$(find "${COMPOSE_DIR}"/../backups/* | grep "${COMMAND_ARGS[${RESTORE_ARGS_LAST_IDX}]}" | head -n 1)
    fi

    RESTORE_DIR="${COMPOSE_DIR}/../backups/"

    #Check if the restore file exists and abort if it does not.
    if [ ! -f "${RESTORE_DIR}/${RESTORE_FILE}" ]; then
    	fatal "Timestamp or restore file not found for args=${RESTORE_ARGS[0]}"
    fi

    #Start restore procedure
    info "Restoring (args=${RESTORE_ARGS}), restore file=${RESTORE_FILE}"
    shell_args="COMPOSE_DIR=${COMPOSE_DIR} BACKUPS_DIR=${RESTORE_DIR} RESTORE_FILE=${RESTORE_FILE}"
    _run_command "restore" "${shell_args} ${RESTORE_SCRIPT} ${RESTORE_ARGS} ${VERBOSE_ARG}"
}

is_timestamp_valid() {
    # Validate timestamp
    if [ -z "${1}" ]; then
        error "Backup timestamp must be supplied"
        return 1
    fi
    if [[ ! "${1}" =~ ^([0-9]+.[0-9]+|latest)$ ]]; then
        error "Invalid timestamp: \"${1}\""
        return 1
    fi
    return 0
}

do_exec() {
    EXEC_NAME=${COMMAND_ARGS[0]}
    EXEC_CMD=""
    for COMMAND_ARG in "${COMMAND_ARGS[@]:1}"
    do
        if [ "${EXEC_CMD}" == "" ]; then
            EXEC_CMD="${COMMAND_ARG}"
        else
            EXEC_CMD="${EXEC_CMD} ${COMMAND_ARG}"
        fi
    done

    if [ "${EXEC_NAME}" == "" ]; then
        fatal "exec service name parameter is required"
    fi
    if [ "${EXEC_CMD}" == "" ]; then
        fatal "exec command parameter is required"
    fi

    COMPOSE_CMD_ARGS="${EXEC_NAME} ${EXEC_CMD}"
    _run_compose_command "exec -T"
}

lint() {
    RETURN=0

    #Source linting setup
    if [ -f ".linting" ]; then
      set -a
      source .linting set
      set +a
    fi

    if [ -f "${LINT_SCRIPT}" ]; then
        info "Running custom lint script: ${LINT_SCRIPT}"
        _run_command "custom lint" "${LINT_SCRIPT} ${VERBOSE_ARG}"
    else
        info "Running generic lint command, cwd=$(pwd)"
        info "**** Linting image shell scripts ****"

        #Set linting prune options
        LINT_PRUNE_OPTS=""
        if [ "${LINT_SHELL_EXCLUDE_DIRS}" != "" ]; then
          dirs=""
          IFS=";"
          for dir_to_exclude in ${LINT_SHELL_EXCLUDE_DIRS}; do
            printf "dir_to_exclude: %s\n" "${dir_to_exclude}"
            if [ "${dirs}" == "" ]; then
              dirs="-name \"${dir_to_exclude}\""
            else
              dirs="${dirs} -o -name \"${dir_to_exclude}\""
            fi
          done
          LINT_PRUNE_OPTS="-type d \\( ${dirs} \\) -prune"
          printf "LINT_PRUNE_OPTS=[%s]\n" "${LINT_PRUNE_OPTS}"
        fi

        CMD="docker run -t --rm -v /var/run/docker.sock:/var/run/docker.sock \
            -v "${COMPOSE_DIR}:/project" "${_SHELL_LINT_IMAGE}:${_SHELL_LINT_IMAGE_VERSION}"\
            sh -c \"find ${LINT_PRUNE_OPTS} /project  \( -name *.sh -o -name *.bash \) -exec \
            sh -c 'touch /tmp/exitcode; echo \*\* Linting: {} \*\*; if ! shellcheck {} --color=always --exclude SC1091; then echo 1 >>/tmp/exitcode; else echo 0 >>/tmp/exitcode; fi; echo \*\* Done \*\*' \;
            return \\\$(awk '{ sum += \\\$1 } END { print sum }' /tmp/exitcode);\""

        _run_command "lint" "${CMD}"
        number_of_failed_scripts=$?

        if [ "${number_of_failed_scripts}" -eq 0 ]
        then
            info "**** Success ****"
        else
            error "Lint failed for ${number_of_failed_scripts} scripts"
            fatal "**** Fail ****"
        fi
    fi
}

scan() {
    EXIT_CODE=0
    if [ -f "${SCAN_SCRIPT}" ]; then
        info "Running custom scan script: ${SCAN_SCRIPT}"
        _run_command "custom scan" "${SCAN_SCRIPT} ${VERBOSE_ARG}"
    else
        info "Running generic scan command, cwd=$(pwd)"
        info "Configured docker-compose files: ${COMPOSE_OPTS//-f} ..."

        info "**** Scanning for vulnerabilities ****"
        for file in ${COMPOSE_OPTS//-f}
        do
            filepath="${COMPOSE_CLARIN_DIR}/${file}"
            if [ -f "${filepath}" ]; then
                info "Searching for images in $COMPOSE_CLARIN_DIR/$file"
                while read -r image
                do
                    if [ ! "${image}" == "null" ]; then
                        info "**** Scanning image ****"
                        info "Image name: ${image}"
                        
                        CMD="docker run -t --rm -e "SNYK_TOKEN=${SNYK_TOKEN}" -v "${COMPOSE_DIR}:/project" \
                            "${_SECURITY_SCAN_IMAGE}:${_SECURITY_SCAN_IMAGE_VERSION}" container test $image"

                        if ! _run_command "scan" "${CMD}"
                        then
                            error "Security scan FAILED for ${image}"
                            EXIT_CODE=1
                        fi
                    else
                        warn "No images declared in ${filepath}"
                    fi
                done <<< "$(docker run --rm -v "${COMPOSE_CLARIN_DIR}":"/project_clarin" "${_YQ_IMAGE}:${_YQ_IMAGE_VERSION}" eval .services.*.image "/project_clarin/${file}")"
            else
                error "Declared overlay file in: ${filepath} does not exist."
                EXIT_CODE=1
            fi
        done
    fi
    exit $EXIT_CODE
}

test() {
    if [ -f "${TEST_SCRIPT}" ]; then
        info "Running custom test script: ${TEST_SCRIPT}"
        _run_command "custom test" "${TEST_SCRIPT} ${VERBOSE_ARG}"
    else
        info "Running generic test command, cwd=$(pwd)"
        info "**** Testing image ****"
        if [ ! -d "${TEST_DIR}" ]; then
          fatal "Test directory (${TEST_DIR}) not found"
        fi
        if [ ! -f "${COMPOSE_OVERRIDE_TEST_FILE}" ]; then
          fatal "compose-test-override.yml not found in test directory (${TEST_DIR})"
        fi

        if [ "${OVERLAYS_TEST_FILE}" ]; then
          info "Reading supplied test .overlays file from test directory (${TEST_DIR})"
          setup_overlays "${OVERLAYS_TEST_FILE}"
        fi

        #Some CLARIN projects require host postfix network
        TEST_NETWORK=0
        POSTFIX_NETWORKNAME=postfix_mail
        if [ "$(docker network ls |grep -c "${POSTFIX_NETWORKNAME}")" -eq 0 ]; then
            _run_command "test" "docker network create ${POSTFIX_NETWORKNAME}"
            TEST_NETWORK=1
        fi

        #Set compose to load the test .env and compose overlay files
        COMPOSE_OPTS+=" -f $(realpath "${COMPOSE_OVERRIDE_TEST_FILE}")"
        COMPOSE_OPTS+=" --env-file $(realpath "${TEST_DIR}")/.env"

        #Make sure to stop an eventual previous run
        stop

        #Run test.
        start test

        #Verify all containers are closed nicely
        
        number_of_failed_containers=0
        number_of_failed_containers="$(
            cd "${COMPOSE_CLARIN_DIR}"|| exit 1
            read -r -a opts_array < <(echo "${COMPOSE_OPTS}")
            docker-compose "${opts_array[@]}" ps -q -a | xargs docker inspect -f '{{ .State.ExitCode }}' | grep -c 0 -v | tr -d ' '
        )"

        #cleanup
        COMPOSE_CMD_ARGS="-v"
        stop
        if [ "$TEST_NETWORK" -eq 1 ]; then
            _run_command "test" "docker network rm ${POSTFIX_NETWORKNAME}"
        fi

        exit "$number_of_failed_containers"

    fi
}

debug() {
    if [ "${VERBOSE}" -eq 1 ]; then
        tag="${2}"
        if [ "${2}" == "" ]; then
                tag="default"
        fi
        log "DEBUG" "${1}" "${tag}"
    fi
}

info() {
    tag="${2}"
    if [ "${2}" == "" ]; then
        tag="default"
    fi
    log "INFO" "${1}" "${tag}"
}

warn() {
    tag="${2}"
    if [ "${2}" == "" ]; then
        tag="default"
    fi
    log "WARN" "${1}" "${tag}"
}

error() {
    tag="${2}"
    if [ "${2}" == "" ]; then
        tag="default"
    fi
    log "ERROR" "${1}" "${tag}"
}

fatal() {
    tag="${2}"
    if [ "${2}" == "" ]; then
        tag="default"
    fi
    log "FATAL" "${1}" "${tag}"
    exit 1
}

log() {
    timestamp=$(date '+%Y-%m-%d %H:%M:%S')
    lvl="$1"
    msg="$2"
    tag="$3"
    log_context="${BASH_SOURCE[0]}"
    if [ "${log_context}" ] && [ "${VERBOSE}" -eq 1 ]; then
        log_context="$(printf '%s:%d' "${log_context}" "${BASH_LINENO[1]}")"
    fi
    echo -e "[${timestamp}] [${lvl}] [${tag}] [${log_context}] ${msg}"

}

ask_confirm() {
    read -r -p "${1} Continue: [y/N] " response
    if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
    then
        info "Continuing"
    else
        info "Aborting"
        exit 1
    fi
}

_run_compose_command() {
    COMPOSE_CMD=$1
    debug "docker-compose command=[docker-compose ${COMPOSE_OPTS} ${COMPOSE_CMD} ${COMPOSE_CMD_ARGS}]"
    #Capture all output from docker-compose (stderr redirected to stdout) and print via the
    #info function to add timestamps
    (
        read -r -a opts_array < <(echo "${COMPOSE_OPTS} ${COMPOSE_CMD} ${COMPOSE_CMD_ARGS}")
        cd "${COMPOSE_CLARIN_DIR}" &&
        stdbuf -oL docker-compose "${opts_array[@]}" 2>&1 |
        while IFS= read -r line
        do
            info "$line" "compose"
        done
    )
}

_run_command() {
    TAG=$1
    CMD=$2
    debug "${CMD}"
    stdbuf -oL bash -c "${CMD}" 2>&1 |
    while IFS= read -r line
    do
        info "${line}" "${TAG}"
    done
    CMD_STATUS="${PIPESTATUS[0]}"
    debug "Command status for '${CMD}': ${CMD_STATUS}"
    return "${CMD_STATUS}"
}

lsbackups() {
    info "Available backup files:"
    if [ -f "${LIST_BACKUPS_SCRIPT}" ]; then
        ${LIST_BACKUPS_SCRIPT} "${VERBOSE_ARG}"
    else
        generic_lsbackups
    fi
}

generic_lsbackups() {
    ls_opts="-T"
    if [[ $(uname) == 'Linux' ]]; then
        ls_opts="--time-style=\"+%b %d %H:%M:%S %Y\""
    fi

    (cd "${COMPOSE_DIR}"/.. || exit 1
    echo ""
    printf "%-15s %-47s %s\n" "Date:" "File name:" "Timestamp:"
    find -L backups -type f -exec bash -c \
    'ls -l '"$ls_opts"' $1' shell {} \; | awk -F ' ' '{
     filepath=$10;
     printf "%-7s", $6 " " $7 ;
     printf "%-9s", $9 ;
     sub(/^backups\/.+\//,"",$10); printf "%-48s", $10 ;
     match($10, /([0-9]+T?[0-9]+)/); printf "%-15s\n", substr($10, RSTART, RLENGTH);}' \
    | sort  -k3,3nr -k1,1Mr -k2,2nr -k5,5r -k4,4r | sed "1s/\(.*\)/\1 (latest)/"
    echo ""
    echo "Restore syntax:"
    echo "- control.sh [<name>|-s] restore [timestamp|filename|'latest']"
    echo ""
    echo "Examples:"
    echo "- control.sh [<name>|-s] restore <filename>       Restore the backup file identified by the file name <filename>."
    echo "- control.sh [<name>|-s] restore <timestamp>      Restore the backup file identified by its <timestamp> as displayed"
    echo "                                                    by \`control.sh restore ls\`."
    echo "- control.sh [<name>|-s] restore latest           Restore the lastest backup available. Support depends on the project's"
    echo "                                                    \`restore.sh\` script implementation."
    echo ""
    echo "")
}

main "$@"; exit
